#pragma once

#include <memory> // unique_ptr, shared_ptr
#include <utility> // pair
#include <list>  // list

#include "cocos2d.h"

// #include "game/MapLoader.h"

class SixCatsLogger;

class MapInformation {
public:
MapInformation();
~MapInformation();

cocos2d::Size getMapSize() const;

// returns true if there is obstacle at given game coordinates
bool getObstacleAt(const int x, const int y);
void setObstacleAt(const int x, const int y);

std::list< std::pair<int, int> > getBarrelsPositions() const;

cocos2d::Vec2 getSwordsmanStart() const;
cocos2d::Vec2 getSpiderStart() const;

protected:

friend class MapLoader;
void setMapSize(const int width, const int height);
void addBarrelPosition(const int x, const int y);
void setSpiderStart(const int x, const int y);
void setSwordsmanStart(const int x, const int y);


std::unique_ptr<SixCatsLogger>c6;

bool* obstaclesMap;

int mapWidth;
int mapHeight;

int swordsmanStartX;
int swordsmanStartY;

std::list< std::pair<int, int> > barrelsPositions;

int spiderStartX;
int spiderStartY;


};
