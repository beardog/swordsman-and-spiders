#pragma once

#include "cocos2d.h"

enum PersonMoveDirection {
  PERSON_MOVE_UP = 0,
  PERSON_MOVE_DOWN,
  PERSON_MOVE_LEFT,
  PERSON_MOVE_RIGHT
};

class MoveOption {
public:
PersonMoveDirection direction;
float duration;
cocos2d::Vec2 newPosition;
};