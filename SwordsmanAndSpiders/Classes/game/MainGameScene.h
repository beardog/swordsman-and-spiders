#pragma once

#include <memory> // unique_ptr, shared_ptr
#include "cocos2d.h"

#include "BasicPerson.h"
class SixCatsLogger;
class MapInformation;
class MapLoader;
class SwordsmanNode;
class SpiderNode;
class GameStateManager;


// --- --------------------------------------------------------------------

class MainGameScene : public cocos2d::Scene {
public:

static cocos2d::Scene* createScene();

virtual bool           init();

// implement the "static create()" method manually
CREATE_FUNC(MainGameScene);

protected:
MainGameScene();
virtual ~MainGameScene();


cocos2d::TMXTiledMap* mapNode;
MapInformation* mapInfo;

cocos2d::Size sceneSize;

bool initBackground();

bool initKeyboardProcessing();
void onKeyPressedScene(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

bool initTiledMap();
bool initDebugMap();

// cocos2d::Vec2 realCoordinateToGame(const cocos2d::Vec2& realPos) const;
// cocos2d::Vec2 gameCoordinateToReal(const cocos2d::Vec2& gamePos) const;
// float mapCellSizeHalf; // a size of one game cell (note may be not the same as tile cell)

// void processMoveRequest(const PersonMoveDirection moveDirection);


std::unique_ptr<GameStateManager> gameStateManager;

std::unique_ptr<SixCatsLogger>c6;
};
