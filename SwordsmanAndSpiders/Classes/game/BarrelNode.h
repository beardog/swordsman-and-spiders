#pragma once

#include <memory> // unique_ptr, shared_ptr

#include "cocos2d.h"

#include "BasicPerson.h"
class SixCatsLogger;

class BarrelNode : public cocos2d::Sprite {
public:

void doStayIdle();
void doDie(const cocos2d::CallFunc *const onActionFinished);

bool isDestroyed() const;

static bool loadAnimations();
static bool unloadAnimations();

static BarrelNode* create();

protected:
BarrelNode();
virtual ~BarrelNode();

bool destroyed;

std::unique_ptr<SixCatsLogger>c6;

bool initSelf();
};