#pragma once

#include <memory> // unique_ptr, shared_ptr
#include "cocos2d.h"

class SixCatsLogger;

class MapLoader {
public:
MapLoader();
~MapLoader();

bool load();
std::string getErrorMessage() const;

cocos2d::TMXTiledMap* getMapNode();
MapInformation* getMapInformation();

protected:

bool loadMapInformation();

cocos2d::TMXTiledMap* mapNode;
MapInformation* mapInfo;
std::string errorMessage;

std::unique_ptr<SixCatsLogger>c6;
};