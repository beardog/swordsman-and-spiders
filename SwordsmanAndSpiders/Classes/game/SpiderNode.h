#pragma once

#include <memory> // unique_ptr, shared_ptr

#include "cocos2d.h"

#include "BasicPerson.h"
class SixCatsLogger;

enum SpiderActionStatus {
  SAS_WALKING = 0,
  SAS_ATTACKING,
  SAS_DYING
};

class SpiderNode : public cocos2d::Sprite {
public:

SpiderActionStatus actionStatus;

void doAttack(const PersonMoveDirection moveDirection,
              const cocos2d::CallFunc *const onMoveFinished);
void doDie(const cocos2d::CallFunc *const onActionFinished);

// here newPos is in real coordinates
void doMove(const cocos2d::Vec2 newPos,
            const PersonMoveDirection moveDirection, const float durationModifier,
            const cocos2d::CallFunc *const onMoveFinished);

static bool loadAnimations();

static bool unloadAnimations();

static SpiderNode* create();

protected:
SpiderNode();
virtual ~SpiderNode();

std::unique_ptr<SixCatsLogger>c6;

bool initSelf();
};