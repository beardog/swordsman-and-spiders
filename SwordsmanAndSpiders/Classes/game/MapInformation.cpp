#include "MainGameScene.h"

#include "common/SixCatsLogger.h"
#include "game/MapInformation.h"
#include "game/MetaTileCode.h"

using namespace cocos2d;
using namespace std;

const string metaLayerName = "meta_info";

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

MapInformation::MapInformation() :
  mapWidth(0), mapHeight(0),
  swordsmanStartX(0), swordsmanStartY(0), spiderStartX(0), spiderStartY(0) {
  obstaclesMap = nullptr;

  c6 = make_unique<SixCatsLogger>(SixCatsLogger::Debug);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

MapInformation::~MapInformation() {
  delete obstaclesMap;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void MapInformation::addBarrelPosition(const int x, const int y) {
  barrelsPositions.push_back(make_pair(x,y));
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Size MapInformation::getMapSize() const {
  return Size(mapWidth, mapHeight);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool MapInformation::getObstacleAt(const int x, const int y) {
  if ((x<0) || (x>=mapWidth) ||(y<0) || (y>=mapHeight)) {
    // c6->w(__c6_MN__, [x, y](ostringstream& ss) {
    //   ss << "Received bad value '" << x << ":" << y;
    // });
    return true;
  }

  return obstaclesMap[y*mapWidth + x];
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

list<pair<int, int> > MapInformation::getBarrelsPositions() const {
  return barrelsPositions;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Vec2 MapInformation::getSpiderStart() const {
  // return Vec2(4, 14);//
  return Vec2(spiderStartX, spiderStartY);//
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Vec2 MapInformation::getSwordsmanStart() const {
  // return Vec2(4, 10);//
  return Vec2(swordsmanStartX, swordsmanStartY);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void MapInformation::setMapSize(const int width, const int height) {
  mapWidth = width;
  mapHeight = height;
  if (obstaclesMap) { //this is never expected
    delete obstaclesMap;
  }
  obstaclesMap = new bool[width*height];
  memset(obstaclesMap, 0, (width*height));
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void MapInformation::setObstacleAt(const int x, const int y) {
  if ((x<0) || (x>=mapWidth) ||(y<0) || (y>=mapHeight)) {
    c6->w(__c6_MN__, [x, y](ostringstream& ss) {
      ss << "Received bad value '" << x << ":" << y;
    });
    return;
  }

  obstaclesMap[y*mapWidth + x] = true;
  c6->t(__c6_MN__, [x, y](ostringstream& ss) {
    ss << "Set obstacle at " << x << ":" << y;
  });
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void MapInformation::setSwordsmanStart(const int x, const int y) {
  swordsmanStartX = x;
  swordsmanStartY = y;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void MapInformation::setSpiderStart(const int x, const int y) {
  spiderStartX = x;
  spiderStartY = y;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
