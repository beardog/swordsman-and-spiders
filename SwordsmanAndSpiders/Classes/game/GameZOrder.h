#pragma once

enum z_orders {
  ZO_BACKGROUND = 0,
  ZO_ITEMS,   // barrels and other effects
  ZO_PERSONS, // swordsman and spiders
  ZO_MENU,
  ZO_DEBUG_ITEMS // debug markers
};
