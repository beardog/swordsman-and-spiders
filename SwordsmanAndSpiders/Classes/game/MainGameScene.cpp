#include "MainGameScene.h"

#include "common/SixCatsLogger.h"
#include "game/GameZOrder.h"
#include "game/GameStateManager.h"
#include "game/MetaTileCode.h"
#include "game/MapInformation.h"
#include "game/MapLoader.h"
#include "game/SpiderNode.h"
#include "game/SwordsmanNode.h"

#include "cmath"

using namespace cocos2d;
using namespace std;

static const string backgroundImageFileName = "ui/Background_Large3.png";

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

MainGameScene::MainGameScene() :
  mapInfo(nullptr), mapNode(nullptr) {
  c6 = make_unique<SixCatsLogger>(SixCatsLogger::Debug);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

MainGameScene::~MainGameScene() {
  //
  delete mapInfo;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Scene * MainGameScene::createScene() {
  return MainGameScene::create();
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

// on "init" you need to initialize your instance
bool MainGameScene::init() {
  //////////////////////////////
  // 1. super init first
  if (!Scene::init()) {
    return false;
  }

  // --- constructor things go here
  sceneSize = Director::getInstance()->getVisibleSize();
  gameStateManager = make_unique<GameStateManager>();

  // --- background
  if (!initBackground()) {
    return false;
  }

  // --- tiled map (and all levels information)
  if (!initDebugMap()) {
    return false;
  }

  if (!initTiledMap()) {
    return false;
  }

  if (!gameStateManager->initPersons()) {
    return false;
  }

  // --- keyboard
  if (!initKeyboardProcessing()) {
    return false;
  }

  // ---
  gameStateManager->startGame();

  // --- finally
  return true;
}

// --- --------------------------------------------------------------------

bool MainGameScene::initBackground() {
  c6->t(__c6_MN__, "here");
  Sprite *sprite = Sprite::create(backgroundImageFileName);

  if (sprite == nullptr) {
    c6->c(__c6_MN__, [](ostringstream& ss) {
      ss << "Failed to load '" << backgroundImageFileName << "'file";
    });
    return false;
  }

  // --- else
  sprite->setPosition(Vec2(sceneSize.width / 2, sceneSize.height / 2 ));

  // add the sprite as a child to this layer
  addChild(sprite, 0);

  return true;
}

// --- --------------------------------------------------------------------

bool MainGameScene::initDebugMap() {
  SpriteFrameCache* const sfc = SpriteFrameCache::getInstance();

  const string plistFilename = "mapDebug/map_debug_marks.plist";

  sfc->addSpriteFramesWithFile(plistFilename);
  if (!sfc->isSpriteFramesWithFileLoaded(plistFilename)) {
    c6->c( __c6_MN__, [plistFilename](ostringstream& ss) {
      ss << "Failed to load '" << plistFilename << "'file";
    });
    return false;
  }

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool MainGameScene::initKeyboardProcessing() {
  // keyboard processing
  EventListenerKeyboard* sceneKeyboardListener = EventListenerKeyboard::create();
  sceneKeyboardListener->onKeyPressed = CC_CALLBACK_2(MainGameScene::onKeyPressedScene, this);
  _eventDispatcher->addEventListenerWithSceneGraphPriority(sceneKeyboardListener, this);

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool MainGameScene::initTiledMap() {
  unique_ptr<MapLoader> loader = std::make_unique<MapLoader>();

  if (!loader->load()) {
    c6->c(__c6_MN__, loader->getErrorMessage());
    return false;
  }

  mapNode = loader->getMapNode();
  mapNode->setAnchorPoint(Vec2(0.5, 0.5));
  mapNode->setPosition(Vec2(sceneSize.width / 2, sceneSize.height /2 ));
  addChild(mapNode);

  // gameStateManager->setGameNode(mapNode);

  mapInfo = loader->getMapInformation();
  const Size mapGameSize = mapInfo->getMapSize();
  const Size mapContentSize = mapNode->getContentSize();
  // mapCellSizeHalf = mapContentSize.height / (mapGameSize.height+1);

  gameStateManager->setMapInformation(mapInfo, mapNode);


  c6->d( __c6_MN__, [this, mapContentSize, mapGameSize](
           ostringstream& ss) {
    ss << "Map game size is " << mapGameSize.width << "x" << mapGameSize.height << std::endl;
    ss << "Map content size is " << mapContentSize.width << "x" << mapContentSize.height << std::endl;
    // ss << "Map Cell size half is" << this->mapCellSizeHalf;
  });

  // for (int i = 0; i<(mapGameSize.width); i++) {
  //   for (int j = 0; j<(mapGameSize.height); j++) {
  //     const bool hasObstacle = mapInfo->getObstacleAt(i,j);

  //     if (hasObstacle) {
  //       // const string sqName = tb ? "no_go_sq.png" : "go_sq.png";
  //       const string sqName = "no_go_sq.png";
  //       Sprite* sp = Sprite::createWithSpriteFrameName(sqName);
  //       // sp->setPosition(Vec2(squareSize + (i*squareSize), squareSize + (j*squareSize)));
  //       sp->setPosition(gameCoordinateToReal(Vec2(i,j)));
  //       mapNode->addChild(sp, ZO_DEBUG_ITEMS);
  //     }
  //   }
  // }


  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void MainGameScene::onKeyPressedScene(EventKeyboard::KeyCode keyCode, Event *) {
  c6->d( __c6_MN__, [keyCode](ostringstream& ss) {
    ss << "Key '" << (int)keyCode << "' was pressed";
  });

  if (gameStateManager->processKeyCode(keyCode)) {
    return;
  }

  //else if (EventKeyboard::KeyCode::KEY_K == keyCode) {
  //   printf("%s: K was pressed, do idle animation\n", __func__);
  //   Animation* animation = AnimationCache::getInstance()->getAnimation(idleAnimationName);
  //   Animate* animate = Animate::create(animation);
  //   skeleton->runAction(animate);
  // }
  if (EventKeyboard::KeyCode::KEY_X == keyCode) {
    c6->d(__c6_MN__, "Need to get out.");

    // Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();
  }
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
