#include "game/SpiderNode.h"

#include "common/SixCatsLogger.h"


using namespace cocos2d;
using namespace std;

enum ActionTags {
  AT_IDLE = 0,
  AT_MOVE_ANIM,
  AT_ATTACK_ANIM,
  AT_MOVE
};

struct SpiderAnimationNamesConst {
  string die;

  string attack_up;
  string attack_down;
  string attack_left;
  string attack_right;

  string run_up;
  string run_down;
  string run_left;
  string run_right;
};

static const SpiderAnimationNamesConst animationNames = {
  .die = "spider_black_die",
  .attack_up = "spider_black_attack_up",
  .attack_down = "spider_black_attack_down",
  .attack_left = "spider_black_attack_left",
  .attack_right = "spider_black_attack_right",

  .run_up = "spider_black_move_up",
  .run_down = "spider_black_move_down",
  .run_left = "spider_black_move_left",
  .run_right = "spider_black_move_right"
};

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

SpiderNode::SpiderNode() {
  //
  c6 = make_unique<SixCatsLogger>(SixCatsLogger::Debug);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

SpiderNode::~SpiderNode() {
  c6->d(__c6_MN__, "here");
  //
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

SpiderNode* SpiderNode::create() {
  SpiderNode *result = new (std::nothrow) SpiderNode();
  if (result && result->initSelf()) {
    result->autorelease();
    return result;
  }
  CC_SAFE_DELETE(result);
  return nullptr;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void SpiderNode::doAttack(const PersonMoveDirection moveDirection,
                          const cocos2d::CallFunc *const onMoveFinished) {
  string animationName = "";//
  switch (moveDirection) {
  case PERSON_MOVE_UP:
    animationName = animationNames.attack_up;
    break;
  case PERSON_MOVE_DOWN:
    animationName = animationNames.attack_down;
    break;
  case PERSON_MOVE_LEFT:
    animationName = animationNames.attack_left;
    break;
  case PERSON_MOVE_RIGHT:
    animationName = animationNames.attack_right;
    break;

  default:
    c6->w(__c6_MN__, "Bad moveDirection value");
    return;
  }

  stopAllActions();

  Animation* animation = AnimationCache::getInstance()->getAnimation(animationName);
  Animate* animate = Animate::create(animation);

  runAction(Sequence::create(animate, onMoveFinished, nullptr));

  actionStatus = SAS_ATTACKING;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void SpiderNode::doDie(const CallFunc *const onActionFinished) {
  stopAllActions();

  Animation* animation = AnimationCache::getInstance()->getAnimation(animationNames.die);
  Animate* animate = Animate::create(animation);
  // runAction(animate);

  FadeOut* fo = FadeOut::create(2.0);

  Sequence* seq = Sequence::create(animate, fo, onActionFinished, nullptr);

  runAction(seq);

  actionStatus = SAS_DYING;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void SpiderNode::doMove(const Vec2 newPos, const PersonMoveDirection moveDirection,
                        const float durationModifier, const CallFunc *const onMoveFinished) {
  string moveAnimationName = "";//
  switch (moveDirection) {
  case PERSON_MOVE_UP:
    moveAnimationName = animationNames.run_up;
    break;
  case PERSON_MOVE_DOWN:
    moveAnimationName = animationNames.run_down;
    break;
  case PERSON_MOVE_LEFT:
    moveAnimationName = animationNames.run_left;
    break;
  case PERSON_MOVE_RIGHT:
    moveAnimationName = animationNames.run_right;
    break;

  default:
    c6->w(__c6_MN__, "Bad moveDirection value");
    return;
  }

  stopAllActions();

  Animation* animation = AnimationCache::getInstance()->getAnimation(moveAnimationName);
  Animate* animate = Animate::create(animation);
  animate->setTag(AT_MOVE_ANIM);
  runAction(RepeatForever::create(animate));

  const float basicMoveDuration = 2.0;

  MoveTo* mta = MoveTo::create(basicMoveDuration*durationModifier, newPos);
  mta->setTag(AT_MOVE);
  runAction(Sequence::create(mta, onMoveFinished, nullptr));

  actionStatus = SAS_WALKING;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool SpiderNode::initSelf() {
  // note, it's important to init with some file,
  //animation will not be displayed correctly if this is not done
  initWithSpriteFrameName("anim_spider_black_die_0.png");

  actionStatus = SAS_WALKING;

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool SpiderNode::loadAnimations() {
  SpriteFrameCache* const sfc = SpriteFrameCache::getInstance();

  const string plistFilename = "spider/spider_images.plist";
  sfc->addSpriteFramesWithFile(plistFilename);
  if (!sfc->isSpriteFramesWithFileLoaded(plistFilename)) {
    return false;
  }

  const string animationsPlistFN = "spider/spider_animations.plist";
  AnimationCache * const ac = AnimationCache::getInstance();
  ac->addAnimationsWithFile(animationsPlistFN);

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool SpiderNode::unloadAnimations() {
  //TODO: implement
  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .