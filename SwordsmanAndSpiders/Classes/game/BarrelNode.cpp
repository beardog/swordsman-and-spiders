#include "game/BarrelNode.h"

#include "common/SixCatsLogger.h"


using namespace cocos2d;
using namespace std;

struct BarrelAnimationNamesConst {
  string die;
  string idle;
};

static const BarrelAnimationNamesConst animationNames = {
  .die = "barrel_die",
  .idle = "barrel_idle"
};

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

BarrelNode::BarrelNode() {
  destroyed = false;
  //
  c6 = make_unique<SixCatsLogger>(SixCatsLogger::Debug);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

BarrelNode::~BarrelNode() {
  c6->d(__c6_MN__, "here");
  //
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

BarrelNode* BarrelNode::create() {
  BarrelNode *result = new (std::nothrow) BarrelNode();
  if (result && result->initSelf()) {
    result->autorelease();
    return result;
  }
  CC_SAFE_DELETE(result);
  return nullptr;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void BarrelNode::doStayIdle() {
  stopAllActions();

  Animation* animation = AnimationCache::getInstance()->getAnimation(animationNames.idle);
  Animate* animate = Animate::create(animation);
  runAction(RepeatForever::create(animate));
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void BarrelNode::doDie(const CallFunc *const onActionFinished) {
  stopAllActions();

  Animation* animation = AnimationCache::getInstance()->getAnimation(animationNames.die);
  Animate* animate = Animate::create(animation);

  Sequence* seq = Sequence::create(animate, onActionFinished, nullptr);

  runAction(seq);

  destroyed = true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool BarrelNode::isDestroyed() const {
  return destroyed;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool BarrelNode::initSelf() {
  initWithSpriteFrameName("anim_barrel_a.png");
  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool BarrelNode::loadAnimations() {
  SpriteFrameCache* const sfc = SpriteFrameCache::getInstance();

  const string plistFilename = "barrel/barrel_images.plist";
  sfc->addSpriteFramesWithFile(plistFilename);
  if (!sfc->isSpriteFramesWithFileLoaded(plistFilename)) {
    return false;
  }

  const string animationsPlistFN = "barrel/barrel_animations.plist";
  AnimationCache * const ac = AnimationCache::getInstance();
  ac->addAnimationsWithFile(animationsPlistFN);

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool BarrelNode::unloadAnimations() {
  //TODO: implement
  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
