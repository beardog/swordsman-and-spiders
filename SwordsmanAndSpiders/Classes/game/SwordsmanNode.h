#pragma once

#include <memory> // unique_ptr, shared_ptr

#include "cocos2d.h"

#include "BasicPerson.h"
class SixCatsLogger;


class SwordsmanNode : public cocos2d::Sprite {
public:

// here newPos is in real coordinates
void doAttack(const PersonMoveDirection moveDirection,
              const cocos2d::CallFunc *const onMoveFinished);
void doMove(const cocos2d::Vec2 newPos,
            const PersonMoveDirection moveDirection, const float durationModifier,
            const cocos2d::CallFunc *const onMoveFinished);
void doStayIdle();
void doDie();

static bool loadAnimations();
static bool unloadAnimations();
static SwordsmanNode* create();

protected:
SwordsmanNode();
virtual ~SwordsmanNode();

std::unique_ptr<SixCatsLogger>c6;

bool initSelf();
};