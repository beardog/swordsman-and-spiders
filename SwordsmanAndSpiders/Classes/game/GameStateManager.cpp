#include "GameStateManager.h"

#include "common/SixCatsLogger.h"
#include "game/GameZOrder.h"
#include "game/BarrelNode.h"
#include "game/MapInformation.h"
#include "game/SpiderNode.h"
#include "game/SwordsmanNode.h"

#include "cmath" //ceil floor fabs

using namespace cocos2d;
using namespace std;

static const int spiderPseudoRandomMoveGeneratorSize = 16;
static int spiderPseudoRandomMoveGeneratorIdx = 0;
static int spiderPseudoRandomMoveGeneratorValues[spiderPseudoRandomMoveGeneratorSize] = {
  0,0,3,0,
  1,3,0,2,
  1,0,0,3,
  0,3,0,0
};


struct AttackEvaluationThresholds {
  float swordsman;
  float blackSpider;
};

static const AttackEvaluationThresholds attackThresholds = {
  .swordsman = 2.3,
  .blackSpider = 1.0
};


// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

GameStateManager::GameStateManager() : mapInfo(nullptr), spider(nullptr), gameNode(nullptr) {

  swordsman = nullptr;
  swordsmanLastMove = PERSON_MOVE_DOWN; // because swordsman stats as idle, and attack down seems
                                        //to be most expected after idle position

  mapCellSizeHalf = 32; //TODO: init properly

  c6 = make_unique<SixCatsLogger>(SixCatsLogger::Debug);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

GameStateManager::~GameStateManager() {
  //
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

MoveOption GameStateManager::calculateMoveOptionUp(const Vec2& currentPosition) const {

  MoveOption result;
  result.direction = PERSON_MOVE_UP;

  result.newPosition.x = round(currentPosition.x);// X remains unchanged

  float tmpf = ceil(currentPosition.y);
  if (tmpf == currentPosition.y) {
    result.newPosition.y = (int)(tmpf +1);
    result.duration = 1.0;
  }
  else {
    result.newPosition.y = tmpf;
    result.duration = tmpf - currentPosition.y;
  }

  return result;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

MoveOption GameStateManager::calculateMoveOptionDown(const Vec2& currentPosition) const {

  MoveOption result;
  result.direction = PERSON_MOVE_DOWN;

  result.newPosition.x = round(currentPosition.x);// X remains unchanged

  float tmpf = floor(currentPosition.y);
  if (tmpf == currentPosition.y) {
    result.newPosition.y = tmpf -1;
    result.duration = 1.0;
  }
  else {
    result.newPosition.y = tmpf;
    result.duration = currentPosition.y - tmpf;
  }

  return result;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

MoveOption GameStateManager::calculateMoveOptionLeft(const Vec2& currentPosition) const {

  MoveOption result;
  result.direction = PERSON_MOVE_LEFT;

  float tmpf = floor(currentPosition.x);
  if (tmpf == currentPosition.x) {
    result.newPosition.x = tmpf -1;
    result.duration = 1.0;
  }
  else {
    result.newPosition.x = tmpf;
    result.duration = currentPosition.x - tmpf;
  }

  result.newPosition.y = (int)round(currentPosition.y);  // remains unchanged

  return result;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

MoveOption GameStateManager::calculateMoveOptionRight(const Vec2& currentPosition) const {

  MoveOption result;
  result.direction = PERSON_MOVE_RIGHT;

  float tmpf = ceil(currentPosition.x);
  if (tmpf == currentPosition.x) {
    result.newPosition.x = (int)(tmpf +1);
    result.duration = 1.0;
  }
  else {
    result.newPosition.x = (int)tmpf;
    result.duration = tmpf - currentPosition.x;
  }

  result.newPosition.y = (int)round(currentPosition.y);  // remains unchanged

  return result;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void GameStateManager::callForNewSpider() {
  spider = SpiderNode::create();

  const Vec2 spos = gameCoordinateToReal(mapInfo->getSpiderStart());//
  c6->t( __c6_MN__, [spos](ostringstream& ss) {
    ss << "Will add spider at '" << spos.x << ":" << spos.y;
  });

  spider->setPosition(spos);
  gameNode->addChild(spider, ZO_PERSONS);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void GameStateManager::evaluateAttackUp() {
  const Vec2 swordsmanPos = realCoordinateToGame(swordsman->getPosition());

  const Vec2 spiderPos = realCoordinateToGame(spider->getPosition());

  if ((fabs(swordsmanPos.x - spiderPos.x) < 1.0) &&
      (spiderPos.y >= swordsmanPos.y) &&
      ((spiderPos.y - swordsmanPos.y)<attackThresholds.swordsman)) {
    CallFunc *cf = CallFunc::create([this]() {
      this->spider->removeFromParent();
      this->callForNewSpider();
    });

    spider->doDie(cf);
  }

  for (BarrelNode* bn: barrels) {
    if (bn->isDestroyed()) {
      continue;
    }

    const Vec2 bp = realCoordinateToGame(bn->getPosition());
    if ((fabs(swordsmanPos.x - bp.x) < 1.0) &&
        ( bp.y >= swordsmanPos.y ) &&
        ((bp.y - swordsmanPos.y)<attackThresholds.swordsman)) {
      bn->doDie(nullptr);
    }
  }
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void GameStateManager::evaluateAttackDown() {
  const Vec2 swordsmanPos = realCoordinateToGame(swordsman->getPosition());

  // attack against spiders
  const Vec2 spiderPos = realCoordinateToGame(spider->getPosition());

  if ((fabs(swordsmanPos.x - spiderPos.x) < 1.0) &&
      (swordsmanPos.y >= spiderPos.y) &&
      ((swordsmanPos.y - spiderPos.y)<attackThresholds.swordsman)) {
    CallFunc *cf = CallFunc::create([this]() {
      this->spider->removeFromParent();
      this->callForNewSpider();
    });

    spider->doDie(cf);
  }

  // attack against barrel
  for (BarrelNode* bn: barrels) {
    if (bn->isDestroyed()) {
      continue;
    }

    const Vec2 bp = realCoordinateToGame(bn->getPosition());
    if ((fabs(swordsmanPos.x - bp.x) < 1.0) &&
        (swordsmanPos.y >= bp.y) &&
        ((swordsmanPos.y - bp.y)<attackThresholds.swordsman)) {
      bn->doDie(nullptr);
    }
  }
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void GameStateManager::evaluateAttackLeft() {
  const Vec2 swordsmanPos = realCoordinateToGame(swordsman->getPosition());

  const Vec2 spiderPos = realCoordinateToGame(spider->getPosition());

  if ((fabs(swordsmanPos.y - spiderPos.y) < 1.0) &&
      (swordsmanPos.x >= spiderPos.x) &&
      ((swordsmanPos.x - spiderPos.x)>attackThresholds.swordsman)) {
    CallFunc *cf = CallFunc::create([this]() {
      this->spider->removeFromParent();
      this->callForNewSpider();
    });

    spider->doDie(cf);
  }

  for (BarrelNode* bn: barrels) {
    if (bn->isDestroyed()) {
      continue;
    }

    const Vec2 bp = realCoordinateToGame(bn->getPosition());
    if ((fabs(swordsmanPos.y - bp.y) < 1.0) &&
        ( swordsmanPos.x >= bp.x ) &&
        ((swordsmanPos.x - bp.x)<attackThresholds.swordsman)) {
      bn->doDie(nullptr);
    }
  }
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void GameStateManager::evaluateAttackRight() {
  const Vec2 swordsmanPos = realCoordinateToGame(swordsman->getPosition());

  const Vec2 spiderPos = realCoordinateToGame(spider->getPosition());

  if ((fabs(swordsmanPos.y - spiderPos.y) < 1.0) &&
      (spiderPos.x > swordsmanPos.x) &&
      ((spiderPos.x - swordsmanPos.x)<attackThresholds.swordsman)) {
    CallFunc *cf = CallFunc::create([this]() {
      this->spider->removeFromParent();
      this->callForNewSpider();
    });

    spider->doDie(cf);
  }

  for (BarrelNode* bn: barrels) {
    if (bn->isDestroyed()) {
      continue;
    }

    const Vec2 bp = realCoordinateToGame(bn->getPosition());
    if ((fabs(swordsmanPos.y - bp.y) < 1.0) &&
        ( bp.x >= swordsmanPos.x ) &&
        ((bp.x - swordsmanPos.x)<attackThresholds.swordsman)) {
      bn->doDie(nullptr);
    }
  }

}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool GameStateManager::evaluateSpiderAttack(PersonMoveDirection& direction) {
  const Vec2 swordsmanPos = realCoordinateToGame(swordsman->getPosition());
  const Vec2 spiderPos = realCoordinateToGame(spider->getPosition());

  if (( fabs(swordsmanPos.y - spiderPos.x) < 1.0) &&
      (swordsmanPos.y > spiderPos.y) &&
      ((swordsmanPos.y - spiderPos.y) < attackThresholds.blackSpider)) {
    direction = PERSON_MOVE_UP;
    return true;
  }

  if (( fabs(swordsmanPos.x - spiderPos.x) < 1.0) &&
      (spiderPos.y > swordsmanPos.y) &&
      ((spiderPos.y - swordsmanPos.y) < attackThresholds.blackSpider)) {
    direction = PERSON_MOVE_DOWN;
    return true;
  }

  if (( fabs(swordsmanPos.y - spiderPos.y) < 1.0) &&
      (swordsmanPos.x < spiderPos.x) &&
      ((spiderPos.x - swordsmanPos.x) < attackThresholds.blackSpider)) {
    direction = PERSON_MOVE_LEFT;
    return true;

  }

  if (( fabs(swordsmanPos.y - spiderPos.y) < 1.0) &&
      (spiderPos.x < swordsmanPos.x) &&
      ((swordsmanPos.x - spiderPos.x) < attackThresholds.blackSpider)) {
    direction = PERSON_MOVE_RIGHT;
    return true;
  }

  return false;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool GameStateManager::initBarrels() {
  if (!BarrelNode::loadAnimations()) {
    return false;
  }

  list< pair<int, int> > barrelsPositions = mapInfo->getBarrelsPositions();

  for( pair<int, int> bp: barrelsPositions) {
    BarrelNode* barrel = BarrelNode::create();
    barrel->setPosition(gameCoordinateToReal(Vec2(bp.first, bp.second)));
    gameNode->addChild(barrel, ZO_ITEMS);
    barrels.push_back(barrel);
  }

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool GameStateManager::initPersons() {
  // check if map was loaded (expected always true)
  if ((mapInfo == nullptr) || (gameNode == nullptr)) {
    return false;
  }

  // spiders
  if (!SpiderNode::loadAnimations()) {
    return false;
  }

  callForNewSpider();

  // swordsman
  if (!initSwordsman()) {
    return false;
  }

  // barrels
  if (!initBarrels()) {
    return false;
  }

  // finally
  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool GameStateManager::initSwordsman() {

  if (!SwordsmanNode::loadAnimations()) {
    return false;
  }

  swordsman = SwordsmanNode::create();

  const Vec2 spos = gameCoordinateToReal(mapInfo->getSwordsmanStart());
  c6->d( __c6_MN__, [spos](ostringstream& ss) {
    ss << "Will add swordsman at '" << spos.x << ":" << spos.y;
  });

  swordsman->setPosition(spos);
  gameNode->addChild(swordsman, ZO_PERSONS);

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Vec2 GameStateManager::gameCoordinateToReal(const Vec2& gamePos) const {
  Vec2 result;
  result.x = mapCellSizeHalf + (gamePos.x * mapCellSizeHalf);
  result.y = mapCellSizeHalf + (gamePos.y * mapCellSizeHalf);
  return result;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Vec2 GameStateManager::realCoordinateToGame(const Vec2& realPos) const {
  Vec2 result;
  result.x = (realPos.x - mapCellSizeHalf) / mapCellSizeHalf;
  result.y = (realPos.y - mapCellSizeHalf) / mapCellSizeHalf;
  return result;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

MoveOption GameStateManager::proposeRandomSpiderMove() {
  const Vec2 currentPosition = realCoordinateToGame(spider->getPosition());

  MoveOption moveOption;

  // try to find next move, but do no more tries than valuse in pseudorng
  for (int i = 0; i<spiderPseudoRandomMoveGeneratorSize; i++) {
    const int randInt = spiderPseudoRandomMoveGeneratorValues[spiderPseudoRandomMoveGeneratorIdx];
    spiderPseudoRandomMoveGeneratorIdx++;
    if (spiderPseudoRandomMoveGeneratorIdx>= spiderPseudoRandomMoveGeneratorSize) {
      spiderPseudoRandomMoveGeneratorIdx = 0;
    }

    switch (randInt) {
    case 0:
      moveOption = calculateMoveOptionUp(currentPosition);
      break;
    case 1:
      moveOption = calculateMoveOptionDown(currentPosition);
      break;
    case 2:
      moveOption = calculateMoveOptionLeft(currentPosition);
      break;
    case 3:
      moveOption = calculateMoveOptionRight(currentPosition);
      break;
    default:
      c6->c(__c6_MN__, "Unexpected Error, sorry");
      return moveOption;
    }

    const int obstacleX = (int)moveOption.newPosition.x;
    const int obstacleY = (int)moveOption.newPosition.y;

    if (!mapInfo->getObstacleAt(obstacleX, obstacleY)) {
      break;
    }
    //else continue cycle with another "random" value
  }

  return moveOption;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void GameStateManager::orderSwordsmanAttack() {

  CallFunc *cf = CallFunc::create([this]() {
    this->swordsman->doStayIdle();
  });

  switch(swordsmanLastMove) {
  case PERSON_MOVE_UP:
    swordsman->doAttack(PERSON_MOVE_UP, cf);
    evaluateAttackUp();
    break;
  case PERSON_MOVE_DOWN:
    swordsman->doAttack(PERSON_MOVE_DOWN, cf);
    evaluateAttackDown();
    break;
  case PERSON_MOVE_LEFT:
    swordsman->doAttack(PERSON_MOVE_LEFT, cf);
    evaluateAttackLeft();
    break;
  case PERSON_MOVE_RIGHT:
    swordsman->doAttack(PERSON_MOVE_RIGHT, cf);
    evaluateAttackRight();
    break;

  default:
    c6->c(__c6_MN__, "Unexpected Error, attack cancelled");
    return;
  }
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void GameStateManager::orderSpiderNextMove() {

  const MoveOption nextMO = proposeRandomSpiderMove();

  CallFunc *cf = CallFunc::create([this]() {
    // this->processFarmerMovementFinish();
    c6->d( __c6_MN__, "move finished");
    this->orderSpiderNextMove();
  });

  const Vec2 realNP = gameCoordinateToReal(nextMO.newPosition);

  spider->doMove(realNP, nextMO.direction, nextMO.duration, cf);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void GameStateManager::orderSpiderNextMoveOld() {
  PersonMoveDirection moveDirection; // = spiderMoveGenerator[spiderMoveGeneratorIdx];
  // spiderMoveGeneratorIdx++;
  // if (spiderMoveGeneratorIdx>= spiderMoveGeneratorSize) {
  // spiderMoveGeneratorIdx = 0;
  // }

  const Vec2 csgPos = realCoordinateToGame(spider->getPosition());
  c6->d( __c6_MN__, [csgPos](ostringstream& ss) {
    ss << "Current spider game position is '" << csgPos.x << ":" << csgPos.y;
  });

  //position of an obstacle that can appear in the direction of  movement
  //integer because all obstacles appear at integer coordinates
  int nextObstacleX = 0;
  int nextObstacleY = 0;

  switch (moveDirection) {
  case PERSON_MOVE_UP: {
    nextObstacleX = (int)round(csgPos.x);// X remains unchanged
    nextObstacleY = (int)round(csgPos.y +1);

  }
  break;
  case PERSON_MOVE_DOWN: {
    nextObstacleX = (int)round(csgPos.x);// X remains unchanged
    nextObstacleY = (int)round(csgPos.y -1);
  }
  break;

  case PERSON_MOVE_LEFT: {
    nextObstacleX = (int)round(csgPos.x -1);
    nextObstacleY = (int)round(csgPos.y);// remains unchanged
  }
  break;

  case PERSON_MOVE_RIGHT: {
    nextObstacleX = (int)round(csgPos.x +1);
    nextObstacleY = (int)round(csgPos.y);// remains unchanged
  }
  break;
    //Note, there is no default here
  }

  c6->d( __c6_MN__, [nextObstacleX, nextObstacleY](ostringstream& ss) {
    ss << "New spider game position is " << nextObstacleX << ":" << nextObstacleY;
  });

  if (mapInfo->getObstacleAt(nextObstacleX, nextObstacleY)) {
    c6->d(__c6_MN__, "There is obstacle at new position");
    return;
  }

  const Vec2 newPos = gameCoordinateToReal(Vec2(nextObstacleX, nextObstacleY));

  CallFunc *cf = CallFunc::create([this]() {
    c6->d( __c6_MN__, "move finished");
    this->orderSpiderNextMove();
  });

  spider->doMove(newPos, moveDirection, 1.0, cf);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool GameStateManager::processKeyCode(const cocos2d::EventKeyboard::KeyCode keyCode) {
  const int arrowKeysCount = 4;
  EventKeyboard::KeyCode arrowKeysCodes[arrowKeysCount] = {
    EventKeyboard::KeyCode::KEY_UP_ARROW, EventKeyboard::KeyCode::KEY_DOWN_ARROW,
    EventKeyboard::KeyCode::KEY_LEFT_ARROW, EventKeyboard::KeyCode::KEY_RIGHT_ARROW
  };

  PersonMoveDirection moveDirections[arrowKeysCount] = {
    PERSON_MOVE_UP,  PERSON_MOVE_DOWN,  PERSON_MOVE_LEFT,  PERSON_MOVE_RIGHT
  };

  for (int i = 0; i<arrowKeysCount; i++) {
    if (arrowKeysCodes[i] == keyCode) {
      processMoveRequest(moveDirections[i]);
      return true;
    }
  }

  if (keyCode == EventKeyboard::KeyCode::KEY_SPACE) {
    c6->d(__c6_MN__,  "Need to do attack\n");
    orderSwordsmanAttack();
  }

  //finally
  return false;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void GameStateManager::processMoveRequest(const PersonMoveDirection moveDirection) {
  const Vec2 csgPos = realCoordinateToGame(swordsman->getPosition());
  c6->d( __c6_MN__, [csgPos](ostringstream& ss) {
    ss << "Current swordsman game position is '" << csgPos.x << ":" << csgPos.y;
  });

  //position of an obstacle that can appear in the direction of  movement
  //integer because all obstacles appear at integer coordinates
  int nextObstacleX = 0;
  int nextObstacleY = 0;

  Vec2 nextPos = csgPos;

  float durationModifier = 1.0;

  switch (moveDirection) {
  case PERSON_MOVE_UP: {
    nextObstacleX = (int)round(csgPos.x);// X remains unchanged

    float tmpf = ceil(csgPos.y);
    if (tmpf == csgPos.y) {
      nextObstacleY = (int)(tmpf +1);
    }
    else {
      nextObstacleY = (int)tmpf;
      durationModifier = tmpf - csgPos.y;
    }
    nextPos.y = nextObstacleY;
  }
  break;
  case PERSON_MOVE_DOWN: {
    nextObstacleX = (int)round(csgPos.x);// X remains unchanged

    float tmpf = floor(csgPos.y);
    if (tmpf == csgPos.y) {
      nextObstacleY = (int)(tmpf -1);
    }
    else {
      nextObstacleY = (int)tmpf;
      durationModifier = csgPos.y - tmpf;
    }
    nextPos.y = nextObstacleY;
  }
  break;

  case PERSON_MOVE_LEFT: {
    nextObstacleY = (int)round(csgPos.y);// remains unchanged

    float tmpf = floor(csgPos.x);
    if (tmpf == csgPos.x) {
      nextObstacleX = (int)(tmpf -1);
    }
    else {
      nextObstacleX = (int)tmpf;
      durationModifier = csgPos.x - tmpf;
    }
    nextPos.x = nextObstacleX;
  }
  break;

  case PERSON_MOVE_RIGHT: {
    nextObstacleY = (int)round(csgPos.y);// remains unchanged

    float tmpf = ceil(csgPos.x);
    if (tmpf == csgPos.x) {
      nextObstacleX = (int)(tmpf +1);
    }
    else {
      nextObstacleX = (int)tmpf;
      durationModifier = tmpf - csgPos.x;
    }
    nextPos.x = nextObstacleX;
  }
  break;
    //Note, there is no default here
  }

  c6->d( __c6_MN__, [nextObstacleX, nextObstacleY, durationModifier, nextPos](ostringstream& ss) {
    ss << "New swordsman game position is " << nextObstacleX << ":" << nextObstacleY;
    ss << "as " << nextPos.x << ":" << nextPos.y << endl;
    ss << "Duration should be " << durationModifier;
  });

  if (mapInfo->getObstacleAt(nextObstacleX, nextObstacleY)) {
    c6->d(__c6_MN__, "There is obstacle at new position");
    return;
  }

  const Vec2 newPos = gameCoordinateToReal(nextPos);

  CallFunc *cf = CallFunc::create([this]() {
    // this->processFarmerMovementFinish();
    c6->d( __c6_MN__, "move finished");
    swordsman->doStayIdle();
  });

  swordsman->doMove(newPos, moveDirection, durationModifier, cf);

  swordsmanLastMove = moveDirection;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void GameStateManager::reevaluatePersons() {

  if ((spider->actionStatus == SAS_ATTACKING)||(spider->actionStatus == SAS_DYING)) {
    return;
  }

  PersonMoveDirection sad;//spider attack direction
  if (evaluateSpiderAttack(sad)) {
    CallFunc *cf = CallFunc::create([this]() {
      c6->d( __c6_MN__, "attack finished");
      this->orderSpiderNextMove();
    });

    spider->doAttack(sad, cf);
    swordsman->doDie();
  }
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void GameStateManager::setMapInformation(MapInformation* inMapInfo, Node * inGameNode) {
  mapInfo = inMapInfo;
  gameNode = inGameNode;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void GameStateManager::startGame() {
  c6->d(__c6_MN__, "Game started");
  orderSpiderNextMove();

  CallFunc *cf = CallFunc::create([this]() {
    c6->f( __c6_MN__, "Reevaluation request is needed");
    this->reevaluatePersons();
  });

  DelayTime* const dt = DelayTime::create(0.5);
  Sequence* const sq = Sequence::create(dt, cf, nullptr);
  gameNode->runAction(RepeatForever::create(sq));

  for (BarrelNode* b: barrels) {
    b->doStayIdle();
  }
}

