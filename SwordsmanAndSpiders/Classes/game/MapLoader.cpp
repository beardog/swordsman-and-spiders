#include "MainGameScene.h"

#include "common/SixCatsLogger.h"
#include "game/MetaTileCode.h"
#include "game/MapInformation.h"
#include "game/MapLoader.h"

using namespace cocos2d;
using namespace std;

static const string defaultMapFileName = "maps/m03.tmx";

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

MapLoader::MapLoader() {
  errorMessage = "";
  mapInfo = nullptr;
  mapNode = nullptr;

  c6 = make_unique<SixCatsLogger>(SixCatsLogger::Debug);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

MapLoader::~MapLoader() {
  delete mapInfo;
  delete mapNode;// TODO: remove this?
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool MapLoader::load() {
  mapNode =  TMXTiledMap::create(defaultMapFileName);

  if (mapNode == nullptr) {
    ostringstream ss;
    ss << "failed to load tiled map from " << defaultMapFileName;
    errorMessage = ss.str();
    return false;
  }

  if (!loadMapInformation()) {
    return false;
  }

  //finally
  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool MapLoader::loadMapInformation() {
  const string metaLayerName = "meta_info";
  TMXLayer *const metaLayer = mapNode->getLayer(metaLayerName);
  if (metaLayer == nullptr) {
    ostringstream ss;
    ss << "Failed to find '" << metaLayerName << "' layer on the map";
    errorMessage = ss.str();
    return false;
  }

  const Size mapSize = mapNode->getMapSize();
  const int obstaclesMapWidth = static_cast<int>(mapSize.width);
  const int obstaclesMapHeight = static_cast<int>(mapSize.height);
  c6->d(__c6_MN__, [obstaclesMapWidth, obstaclesMapHeight](ostringstream& ss) {
    ss << "Map size is" << obstaclesMapWidth<< "x" << obstaclesMapHeight;
  });
  bool* tmpObstaclesMap = new bool[obstaclesMapWidth*obstaclesMapHeight];
  memset(tmpObstaclesMap, 0, (obstaclesMapWidth*obstaclesMapHeight));

  mapInfo = new MapInformation();


  for (int tileX = 0; tileX < obstaclesMapWidth; tileX++) {
    for (int tileY = 0; tileY < obstaclesMapHeight; tileY++) {
      const int tileGid = metaLayer->getTileGIDAt(Vec2(tileX, tileY));
      const Value prop    = mapNode->getPropertiesForGID(tileGid);

      if (prop.isNull()) {
        continue;
      }

      const ValueMap vm   = prop.asValueMap();
      const auto frez = vm.find("MetaTileCode");

      if (frez == vm.end()) {
        c6->d(__c6_MN__, [tileX, tileY](ostringstream& ss) {
          ss << "Skipped, no meta code at '" << tileX << ":" << tileY << "')";
        });
        continue;
      }

      const int metaCode = frez->second.asInt();

      switch (metaCode) {
      case MTC_SnS_UNPASSABLE: {
        const int recalcY = obstaclesMapHeight - tileY - 1;
        tmpObstaclesMap[obstaclesMapWidth*(recalcY) + tileX] = true;
        c6->t(__c6_MN__, [tileX, tileY, recalcY](ostringstream& ss) {
            ss << "Found obstacle at '" << tileX << ":" << recalcY
               << "'(original '" << tileX << ":" << tileY << ")";
          });
      }
      break;

      case MTC_SnS_WARRIOR_START:
        mapInfo->setSwordsmanStart(tileX, obstaclesMapHeight - tileY - 1);
        break;

      case MTC_SnS_SPIDER_START:
        mapInfo->setSpiderStart(tileX, obstaclesMapHeight - tileY - 1);
        break;

      case MTC_SnS_BARREL:
        mapInfo->addBarrelPosition(tileX, obstaclesMapHeight - tileY - 1);
        break;

      // Note there is no suitable default action here
      default:
        c6->d(__c6_MN__, [metaCode](ostringstream& ss) {
          ss << "Skipped unknown value " << metaCode;
        });
      }


    }
  }

  // const int squareSize = static_cast<int>(mapNode->getTileSize().width);
  mapInfo->setMapSize(obstaclesMapWidth-1, obstaclesMapHeight-1 );

  for (int i = 0; i<(obstaclesMapWidth-1); i++) {
    for (int j = 0; j<(obstaclesMapHeight-1); j++) {
      bool tb = tmpObstaclesMap[j*obstaclesMapWidth + i];
      tb = tb || tmpObstaclesMap[j*obstaclesMapWidth + (i+1)];
      tb = tb || tmpObstaclesMap[(j+1)*obstaclesMapWidth + i];
      tb = tb || tmpObstaclesMap[(j+1)*obstaclesMapWidth + (i+1)];

      if (tb) {
        mapInfo->setObstacleAt(i, j);
      }
    }
  }

  delete tmpObstaclesMap;

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

std::string MapLoader::getErrorMessage() const {
  return errorMessage;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

TMXTiledMap* MapLoader::getMapNode() {
  TMXTiledMap* result = mapNode;
  mapNode = nullptr;
  return result;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

MapInformation* MapLoader::getMapInformation() {
  MapInformation* result = mapInfo;
  mapInfo = nullptr;
  return result;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
