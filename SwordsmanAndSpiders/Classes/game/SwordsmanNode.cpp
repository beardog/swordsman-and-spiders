#include "game/SwordsmanNode.h"
// #include "MainGameScene.h"

#include "common/SixCatsLogger.h"

using namespace cocos2d;
using namespace std;

enum ActionTags {
  AT_IDLE = 0,
  AT_MOVE_ANIM,
  AT_MOVE
};

struct AnimationNamesConst {
  string idle;
  string die;
  string run_up;
  string run_down;
  string run_left;
  string run_right;
  string attack_up;
  string attack_down;
  string attack_left;
  string attack_right;
};

static const AnimationNamesConst animationNames = {
  .idle = "swordsman_idle",
  .die = "swordsman_die",

  .run_up = "swordsman_run_up",
  .run_down = "swordsman_run_down",
  .run_left = "swordsman_run_left",
  .run_right = "swordsman_run_right",

  .attack_up = "swordsman_slice_up",
  .attack_down = "swordsman_slice_down",
  .attack_left = "swordsman_slice_right",//this is correct, right and left images are misplaced
  .attack_right = "swordsman_slice_left"
};

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

SwordsmanNode::SwordsmanNode() {
  //
  c6 = make_unique<SixCatsLogger>(SixCatsLogger::Debug);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

SwordsmanNode::~SwordsmanNode() {
  c6->d(__c6_MN__, "here");
  //
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

SwordsmanNode* SwordsmanNode::create() {
  SwordsmanNode *result = new (std::nothrow) SwordsmanNode();
  if (result && result->initSelf()) {
    result->autorelease();
    return result;
  }
  CC_SAFE_DELETE(result);
  return nullptr;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool SwordsmanNode::initSelf() {
  // note, it's important to init with some file,
  //animation will not be displayed correctly if this is not done
  initWithSpriteFrameName("anim_swordsman_idle_0.png");

  doStayIdle();

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void SwordsmanNode::doAttack(const PersonMoveDirection moveDirection,
                             const cocos2d::CallFunc *const onMoveFinished) {
  string animationName = "";//
  switch (moveDirection) {
  case PERSON_MOVE_UP:
    animationName = animationNames.attack_up;
    break;
  case PERSON_MOVE_DOWN:
    animationName = animationNames.attack_down;
    break;
  case PERSON_MOVE_LEFT:
    animationName = animationNames.attack_left;
    break;
  case PERSON_MOVE_RIGHT:
    animationName = animationNames.attack_right;
    break;

  default:
    c6->w(__c6_MN__, "Bad moveDirection value");
    return;
  }

  stopAllActions();

  Animation* animation = AnimationCache::getInstance()->getAnimation(animationName);
  Animate* animate = Animate::create(animation);

  runAction(Sequence::create(animate, onMoveFinished, nullptr));
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void SwordsmanNode::doMove(const Vec2 newPos, const PersonMoveDirection moveDirection,
                           const float durationModifier,
                           const CallFunc *const onMoveFinished) {
  string moveAnimationName = "";//
  switch (moveDirection) {
  case PERSON_MOVE_UP:
    moveAnimationName = animationNames.run_up;
    break;
  case PERSON_MOVE_DOWN:
    moveAnimationName = animationNames.run_down;
    break;
  case PERSON_MOVE_LEFT:
    moveAnimationName = animationNames.run_left;
    break;
  case PERSON_MOVE_RIGHT:
    moveAnimationName = animationNames.run_right;
    break;

  default:
    c6->w(__c6_MN__, "Bad moveDirection value");
    return;
  }

  stopAllActions();

  Animation* animation = AnimationCache::getInstance()->getAnimation(moveAnimationName);
  Animate* animate = Animate::create(animation);
  animate->setTag(AT_MOVE_ANIM);
  runAction(RepeatForever::create(animate));

  const float basicMoveDuration = 1.0;

  MoveTo* mta = MoveTo::create(basicMoveDuration*durationModifier, newPos);
  mta->setTag(AT_MOVE);
  runAction(Sequence::create(mta, onMoveFinished, nullptr));

}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool SwordsmanNode::loadAnimations() {
  SpriteFrameCache* const sfc = SpriteFrameCache::getInstance();

  const string plistFilename = "swordsman/swordsman_images.plist";
  sfc->addSpriteFramesWithFile(plistFilename);
  if (!sfc->isSpriteFramesWithFileLoaded(plistFilename)) {
    return false;
  }

  const string animationsPlistFN = "swordsman/swordsman_animations.plist";
  AnimationCache * const ac = AnimationCache::getInstance();
  ac->addAnimationsWithFile(animationsPlistFN);

  // TODO: check for all animations?

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool SwordsmanNode::unloadAnimations() {
  //TODO: implement
  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void SwordsmanNode::doStayIdle() {
  stopAllActions();

  Animation* animation = AnimationCache::getInstance()->getAnimation(animationNames.idle);
  Animate* animate = Animate::create(animation);
  animate->setTag(AT_IDLE);
  runAction(RepeatForever::create(animate));
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void SwordsmanNode::doDie() {
  stopAllActions();

  Animation* animation = AnimationCache::getInstance()->getAnimation(animationNames.die);
  Animate* animate = Animate::create(animation);
  runAction(RepeatForever::create(animate));
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .