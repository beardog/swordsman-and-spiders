#pragma once

#include <memory> // unique_ptr, shared_ptr
#include "cocos2d.h"

#include "BasicPerson.h"
class SixCatsLogger;
class BarrelNode;
class MapInformation;
class SpiderNode;
class SwordsmanNode;

class GameStateManager final {
public:
GameStateManager();
~GameStateManager();

void setMapInformation(MapInformation* mapInformation, cocos2d::Node* const gameNode);

bool initPersons();

// returns true if key code was processed, false if it should be processed somewhere else
bool processKeyCode(const cocos2d::EventKeyboard::KeyCode keyCode);

void startGame();

protected:
MapInformation* mapInfo;
cocos2d::Node* gameNode;
float mapCellSizeHalf; // a size of one game cell (note may be not the same as tile cell)
cocos2d::Vec2 realCoordinateToGame(const cocos2d::Vec2& realPos) const;
cocos2d::Vec2 gameCoordinateToReal(const cocos2d::Vec2& gamePos) const;

SpiderNode* spider;
void callForNewSpider();

SwordsmanNode* swordsman;
PersonMoveDirection swordsmanLastMove;
bool initSwordsman();

std::list<BarrelNode*> barrels;
bool initBarrels();

// void attackBarrel(BarrelNode* barrelNode);

void processMoveRequest(const PersonMoveDirection moveDirection);

void reevaluatePersons();

void orderSwordsmanAttack();
void evaluateAttackUp();
void evaluateAttackDown();
void evaluateAttackLeft();
void evaluateAttackRight();

// returns true if attack is needed (if attack kills swordsman)
// direction shows attack direction
bool evaluateSpiderAttack(PersonMoveDirection& direction);

void orderSpiderNextMove();
void orderSpiderNextMoveOld();
MoveOption calculateMoveOptionUp(const cocos2d::Vec2& currentPosition) const;
MoveOption calculateMoveOptionDown(const cocos2d::Vec2& currentPosition) const;
MoveOption calculateMoveOptionLeft(const cocos2d::Vec2& currentPosition) const;
MoveOption calculateMoveOptionRight(const cocos2d::Vec2& currentPosition) const;
MoveOption proposeRandomSpiderMove();

std::unique_ptr<SixCatsLogger>c6;
};